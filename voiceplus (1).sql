-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 17. Mei 2018 jam 05:05
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `voiceplus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(8) NOT NULL,
  `nama_adm` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `waktu_msk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hka` varchar(20) NOT NULL DEFAULT 'admin',
  `img` varchar(50) NOT NULL DEFAULT 'img/none.jpg',
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_adm`, `email`, `waktu_msk`, `hka`, `img`) VALUES
(1, 'admin', 'admin', 'Admin Pusat', 'admin@voiceplus.co', '2018-05-16 16:54:12', 'admin', 'img/none.jpg'),
(2, 'andi', 'andi', 'Andi Purbo', 'andipurbo@gmail.com', '2018-05-16 16:54:24', 'admin', 'img/none.jpg'),
(3, 'fazal', 'fazal', 'Fazal Said', 'fsaid1604@gmail.com', '2018-05-16 16:45:37', 'admin', 'img/p1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `det_pembayaran`
--

CREATE TABLE IF NOT EXISTS `det_pembayaran` (
  `no_pembayaran` int(11) NOT NULL,
  `id_murid` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  KEY `id_admin` (`id_admin`),
  KEY `id_murid` (`id_murid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(60) NOT NULL,
  `jk` enum('Laki-Laki','Perempuan') NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `agama` enum('Islam','Kristen','Hindu','Buddha','Katolik') NOT NULL,
  `jenis_kursus` enum('Vokal','Gitar','Piano') NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kursus`
--

CREATE TABLE IF NOT EXISTS `kursus` (
  `jenis_kursus` enum('Vokal','Gitar','Piano') NOT NULL,
  PRIMARY KEY (`jenis_kursus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kursus`
--

INSERT INTO `kursus` (`jenis_kursus`) VALUES
('Gitar'),
('Piano');

-- --------------------------------------------------------

--
-- Struktur dari tabel `murid`
--

CREATE TABLE IF NOT EXISTS `murid` (
  `id_murid` int(11) NOT NULL AUTO_INCREMENT,
  `no_pendaftaran` varchar(10) NOT NULL,
  `nama_murid` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(8) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(60) NOT NULL,
  `alamat` text NOT NULL,
  `jk` enum('Laki-Laki','Perempuan') NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `agama` enum('Islam','Kristen','Hindu','Buddha','Katolik') NOT NULL,
  `pendidikan` enum('SD','SMP','SMA') NOT NULL,
  `jenis_kursus` enum('Vokal','Gitar','Piano') NOT NULL,
  `nama_ortu` varchar(60) NOT NULL,
  `no_telp_ortu` varchar(12) NOT NULL,
  `tanggal` date NOT NULL,
  `hka` varchar(30) NOT NULL DEFAULT 'murid',
  PRIMARY KEY (`id_murid`),
  UNIQUE KEY `no_pendaftaran` (`no_pendaftaran`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `murid`
--

INSERT INTO `murid` (`id_murid`, `no_pendaftaran`, `nama_murid`, `username`, `password`, `tanggal_lahir`, `tempat_lahir`, `alamat`, `jk`, `no_telp`, `email`, `agama`, `pendidikan`, `jenis_kursus`, `nama_ortu`, `no_telp_ortu`, `tanggal`, `hka`) VALUES
(1, '234162', 'Sapri', 'sapri', '123456', '2018-01-09', 'Jogja', 'Jogjakarta', 'Laki-Laki', '082214536241', 'blablabla@gmail.com', 'Islam', 'SD', 'Gitar', 'Giarto', '089763562413', '2018-02-01', 'murid'),
(2, '0003645', 'Andi Purbo', 'andi', '12345678', '2008-05-06', 'Jogja', 'Jogja', 'Laki-Laki', '087635726423', 'blabla@gmail.com', 'Islam', 'SMA', 'Vokal', 'Sartono', '089746536241', '2018-05-17', 'murid'),
(3, '0003646', 'Fazal Said', 'fazal', '123456', '2018-05-16', 'Pacitan', 'Jogjakarta', 'Laki-Laki', '082233439041', 'ichaldotid@gmail.com', 'Islam', 'SMA', 'Gitar', 'Supeno', '083467863725', '2018-05-03', 'murid'),
(4, '0003647', 'Adelia Rismawardani', 'adelia', 'adelia', '1996-04-11', 'Seoul', 'Bussan', 'Perempuan', '085637486273', 'adeliacantik@gmail.com', 'Hindu', 'SMP', 'Vokal', 'Ade Hermana', '089346738645', '2017-12-08', 'murid'),
(9, '0003648', 'saidsaid', 'said', '123', '1996-03-11', 'Pacitan', 'Jogjakarta', 'Laki-Laki', '0987654321', 'haha@mail.com', 'Islam', 'SD', 'Vokal', 'Dedi', '1234567890', '2018-05-05', 'murid'),
(10, '0003649', 'Bintang Toedjoeh', 'bejo', 'bejo', '1990-12-01', 'Bussan', 'Seoul, Korea Selatan', 'Laki-Laki', '097685947586', 'bejokiyut@mail.com', 'Islam', 'SMA', 'Piano', 'Sido Muncul', '083465837465', '2018-05-17', 'murid');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
  `no_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `jumlah_bayar` varchar(10) NOT NULL,
  `nama_murid` varchar(50) NOT NULL,
  `jenis_kursus` enum('Vokal','Gitar','Piano') NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`no_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `det_pembayaran`
--
ALTER TABLE `det_pembayaran`
  ADD CONSTRAINT `det_pembayaran_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`) ON UPDATE CASCADE,
  ADD CONSTRAINT `det_pembayaran_ibfk_2` FOREIGN KEY (`id_murid`) REFERENCES `murid` (`id_murid`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
