<!DOCTYPE html>
<html lang="en">
<head>
<title>Pengumuman - Voiceplus</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Voiceplus" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Sofia" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet"></head>
	
<body>
<!-- banner -->
	<div class="banner1">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h1><a class="navbar-brand" href="index.php">Voiceplus</a></h1>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--iris">
						<ul class="nav navbar-nav menu__list">
                            <li class="menu__item"><a href="index.php" class="menu__link">Beranda</a></li>
                            <li class="menu__item menu__item--current"><a href="pengumuman.php" class="menu__link">Pengumuman</a></li>
							<li class="dropdown menu__item">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Akun <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li><a href="masuk/">Masuk</a></li>
									<li><a href="daftar/">Daftar</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</nav>
		</div>
	</div>
<!-- //banner -->	
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="w3l_breadcrumbs_left">
				<ul>
					<li><a href="index.php">Beranda</a><i>/</i></li>
					<li>Pengumuman</li>
				</ul>
			</div>
			<div class="w3_agile_breadcrumbs_right">
				<h2>Pengumuman</h2>
				<p>Dapatkan informasi terbaru tentang Voiceplus.</p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- music -->
	<div class="about w3_music">
		<div class="container">
			<h3 class="agileits_w3layouts_head">Pengumuman <span>Voiceplus</span></h3>
			<div class="wthree_latest_albums_grids">
                <div class="col-sm-3">
                    <h2>Pengumuman Jadwal</h1><br />
                    <p>Untuk jadwal musik hari ini tidak ada. fsjddsk kjhdkjs kfjhdskk skjfksdjf dkusfkdsjfk ksgfkdsfgsd ksdfdkshgfskd gkdfgskdhfg kgskdfhgs</p>
                </div>
                <div class="col-sm-3">
                    <h2>Pengumuman lain - lain</h1><br />
                    <p>Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. Ini adalah teks. </p>
                </div>
			</div>
		</div>
    </div>
    <br />
<!-- //music -->
<!-- copy-right -->
	<div class="w3agile_copy_right">
		<div class="container">
			 <p>© 2018 Voiceplus. All Rights Reserved.</p>
		</div>
	</div>
<!-- //copy-right -->
<!-- timeline -->
	<script type="text/javascript" src="js/jquery.cntl.js"></script> 
	<script type="text/javascript">
		$(document).ready(function(e){
			$('.cntl').cntl({
				revealbefore: 300,
				anim_class: 'cntl-animate',
				onreveal: function(e){
					console.log(e);
				}
			});
		});
	</script>
<!-- //timeline -->
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>