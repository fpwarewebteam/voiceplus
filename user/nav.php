<nav class="navbar is-transparent">
  <div class="navbar-brand">
    <a class="navbar-item" href="https://fpware.tk">
      <img src="fpware30.png" alt="fpware" width="70px" height="100px">
    </a>
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <div id="navbarExampleTransparentExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="#">
        Profil Saya
      </a>
      <div class="navbar-menu">
        <a class="navbar-item" href="#">
          Tagihan
        </a>
      </div>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="field is-grouped">
        <p class="control">
            <a class="button is-danger" href="#">
              <span class="icon">
                <i class="fas fa-logout"></i>
              </span>
              <span>Fazal Said</span>
            </a>
          </p>
          <p class="control">
            <a class="button is-primary" href="#">
              <span class="icon">
                <i class="fas fa-logout"></i>
              </span>
              <span>Keluar</span>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</nav>