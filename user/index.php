<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
File Tidak Ditemukan
</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
<link rel="stylesheet" type="text/css" href="jquery-ui.css">
<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<script type="text/javascript" src="progressbar.js"></script>
</head>

<center>
<body>
<?php
include 'nav.php'
?>
<section class="section">
<div id="container">
<h1 class="title">Maaf! Halaman tidak ditemukan.</h1>
<h4 class="subtitle">Mungkin Anda tidak meletakkan file <code><b>index.php</b></code> dengan benar.<br />Atau halaman ini masih belum selesai dikerjakan.<br/>Silahkan cek kembali file manager Anda.</h4><br/>
<div class="progress-label">Sudah dimulai</div>
<div id="progressbar3">
</center>
<h6 align="center">Salam,</h6>
<h6 align="center">Developer.</h6>
</div>
</section>
<br/>
<center>
<section class="section">
<div id="container">
<h3 class="subtitle">Jangan lupa untuk kunjungi:</h2>
<h1 class="title"><code><b><a href="http://www.fasadistro.tk">www.fasadistro.tk</a></b></code></h2>
</center>
<br />
<?php
include 'level.php'
?>
<br />
<br />
<br />
<center>
<footer class="footer">
<div class="container">
Copyright 2018. Course. Allright Reserved.
</div>
</footer>
</center>
</body>
</html>